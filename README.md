# Geared Wheel

## Description
Geared wheel est un petit prototype de jeu vidéo développé en C# via le moteur unity durant la Weekly Game JAM 206 (game JAM durant une semaine).
Le thème était "Clockwork".
Vous pouvez y jouer directement dans votre navigateur à l'adresse suivante : https://octodice.itch.io/geared-wheel-post-jam

