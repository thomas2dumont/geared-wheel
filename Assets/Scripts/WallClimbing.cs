using UnityEngine;

public class WallClimbing : MonoBehaviour
{
    public Collider2D boxCollider;

    /*void Update()
    {

        if (!isInRange || (isInRange && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f))
        {
            PlayerMovement.instance.isClimbing = false;
            PlayerMovement.instance.rb.gravityScale = 2;
            return;
        }

        if (isInRange && Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f)
        {
            PlayerMovement.instance.isClimbing = true;
            PlayerMovement.instance.rb.gravityScale = 0;
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == PlayerMovement.instance.GetComponentInParent<CircleCollider2D>())
        {
            PlayerMovement.instance.isClimbing = true;
            PlayerMovement.instance.rb.gravityScale = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == PlayerMovement.instance.GetComponentInParent<CircleCollider2D>())
        {
            PlayerMovement.instance.isClimbing = false;
            PlayerMovement.instance.rb.gravityScale = 2;
        }
    }

}
