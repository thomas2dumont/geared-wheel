using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Transform groundCheck;
    public Transform sprite;
    public LayerMask collisionLayers;

    public float moveSpeed;
    public float climbingSpeed;
    public float jumpForce;
    public float groundCheckRadius = 2f;
    public string mechanismColor = "";

    [HideInInspector]
    public bool isClimbing = false;
    [HideInInspector]
    public bool isInMecha = false;
    [HideInInspector]
    public bool goingNextScene = false;
    [HideInInspector]
    public bool isJumping = false;
    [HideInInspector]
    public float hMove;
    private float vMove;
    private int orientation = 1;
    private Vector3 velocity = Vector3.zero;

    public static PlayerMovement instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("There is more than one PlayerHealth in the scene");
            return;
        }
        instance = this;
    }

    private void Update()
    {
        hMove = Input.GetAxis("Horizontal") * moveSpeed * Time.fixedDeltaTime;
        vMove = Input.GetAxis("Vertical") * climbingSpeed * Time.fixedDeltaTime;

        orientation = Input.GetKeyDown(KeyCode.RightArrow) ? -1 : Input.GetKeyDown(KeyCode.LeftArrow) ? 1 : orientation;
        isJumping = isInMecha && Input.GetKey(KeyCode.UpArrow);
    }

    private void FixedUpdate()
    {
        movePlayer(hMove, vMove);
    }

    private void movePlayer(float _hMove, float _vMove)
    {
        if (!isInMecha && !goingNextScene)
        {
            Vector3 targetVelocity = new Vector2(_hMove, rb.velocity.y);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);
        }
        else
        {
            if (isJumping)
            {
                rb.AddForce(new Vector2(0f, jumpForce));
                isJumping = false;
            }
        }
        if (!goingNextScene)
        {
            sprite.Rotate(Vector3.forward * _hMove * -1f);
        }
        
        if(isClimbing)
        {
            Vector3 targetVelocity = new Vector2(0, _vMove);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);
            sprite.Rotate(Vector3.forward * _vMove * orientation);
        }

    }

}

