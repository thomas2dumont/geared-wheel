using UnityEngine;

public class DoorMovement : MonoBehaviour
{
    public Transform topDoorLimit;
    public Transform DownDoorLimit;
    public Transform gearedWheel;
    public MechaColor doorColor;
    public bool horizontal = false;
    public float moveSpeed = 2f;
    
    [HideInInspector]
    public bool stopMove = true;
    [HideInInspector]
    public bool stopingLimit = false;

    private float vMove;

    private void Start()
    {
        topDoorLimit.parent = null;
        DownDoorLimit.parent = null;
        gearedWheel.parent = null;
    }

    private void Update()
    {
        if (PlayerMovement.instance.isInMecha && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f && PlayerMovement.instance.mechanismColor == doorColor.color)
        {
            vMove = !stopMove ? Input.GetAxisRaw("Horizontal") * -1f : Input.GetAxisRaw("Horizontal") == 1 && stopingLimit == false ? -1 : Input.GetAxisRaw("Horizontal") == -1 && stopingLimit == true ? 1 : 0;
        }
        else if ((PlayerMovement.instance.isInMecha && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 0) || !PlayerMovement.instance.isInMecha)
        {
            vMove = 0;
        }
    }

    private void FixedUpdate()
    {
        moveDoor(vMove);
    }

    private void moveDoor(float _vMove)
    {
        transform.position = !horizontal ?
            Vector3.MoveTowards(transform.position, transform.position + new Vector3(0f, _vMove, 0f), moveSpeed * Time.fixedDeltaTime) :
            Vector3.MoveTowards(transform.position, transform.position + new Vector3(_vMove * -1, 0f, 0f), moveSpeed * Time.fixedDeltaTime);
    }
}
