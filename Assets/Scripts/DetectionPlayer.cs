using System.Collections;
using UnityEngine;

public class DetectionPlayer : MonoBehaviour
{
    public CircleCollider2D circleCollider;
    public AudioClip clip;
    [HideInInspector]
    public GameObject[] gearedWheels;
    [HideInInspector]
    public string mechanismColor = "green";

    private bool isInMecha;
    private AudioSource audioSource;
    private void Update()
    {
        if(isInMecha && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f)
        {
            if(audioSource == null || !audioSource.isPlaying)
            {
                audioSource = AudioManager.instance.PlayClipAt(clip, transform.position);
            }
        }
        else
        {
            if(audioSource != null && audioSource.isPlaying)
            {
                audioSource.Stop();
            }
            else
            {
                audioSource = null;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerMovement.instance.isInMecha = true;
            isInMecha = true;
            PlayerMovement.instance.mechanismColor = mechanismColor;
            foreach (GameObject gearedWheel in gearedWheels)
            {
                gearedWheel.GetComponent<GearedWheelMovement>().shouldMove = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerMovement.instance.isInMecha = false;
            isInMecha = false;
            foreach (GameObject gearedWheel in gearedWheels)
            {
                gearedWheel.GetComponent<GearedWheelMovement>().shouldMove = false;
            }
        }
    }

}
