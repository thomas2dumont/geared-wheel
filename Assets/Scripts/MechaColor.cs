using UnityEngine;

public class MechaColor : MonoBehaviour
{
    public GameObject sprite;
    [HideInInspector]
    public string color = "green";
    public Sprite[] sprites;

    public void Start()
    {
        if(color == "green")
        {
            sprite.GetComponent<SpriteRenderer>().sprite = sprites[0];
        }
        else if(color == "blue")
        {
            sprite.GetComponent<SpriteRenderer>().sprite = sprites[1];

        }
        else if (color == "pink")
        {
            sprite.GetComponent<SpriteRenderer>().sprite = sprites[2];

        }
        else
        {
            Debug.LogWarning("Error color doesn't exists");
        }
    }
}
