using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioClip[] playlist;
    public AudioSource audioSource;
    public AudioMixerGroup soundEffectMixer;
    private int musicIndex = 0;

    public static AudioManager instance;

    private bool audioIsPlaying = false;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("There is more than one AudioManager in the scene");
            return;
        }
        instance = this;
    }

    private void Start()
    {
        audioSource.clip = playlist[0];
        audioSource.Play();
    }

    private void Update()
    {
        if (!audioIsPlaying)
        {
            StartCoroutine(PlayNextSong());
        }
    }

    private IEnumerator PlayNextSong()
    {
        musicIndex = (musicIndex + 1) % playlist.Length;
        audioSource.clip = playlist[musicIndex];
        audioSource.Play();
        audioIsPlaying = true;
        yield return new WaitForSeconds(audioSource.clip.length);
        audioIsPlaying = false;
    }

    public AudioSource PlayClipAt(AudioClip clip, Vector3 pos)
    {
        GameObject tempGO = new GameObject("TempAudio");
        tempGO.transform.position = pos;
        AudioSource audioSource = tempGO.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.outputAudioMixerGroup = soundEffectMixer;
        audioSource.volume = 0.5f;
        audioSource.Play();
        Destroy(tempGO, clip.length);
        return audioSource;
    }
}
