using UnityEngine;

public class MechaHandler : MonoBehaviour
{
    public GameObject[] gearedWheels;
    public GameObject detectionPlayerGO;
    public GameObject[] doors;
    public string color;

    private DetectionPlayer detectionPlayer;

    private void Awake()
    {
        detectionPlayer = detectionPlayerGO.GetComponent<DetectionPlayer>();
        detectionPlayer.mechanismColor = color;
        detectionPlayer.gearedWheels = gearedWheels;
        foreach(GameObject door in doors)
        {
            door.GetComponent<MechaColor>().color = color;
        }
        foreach (GameObject gearedWheel in gearedWheels)
        {
            gearedWheel.GetComponent<MechaColor>().color = color;
        }
    }
}
