using UnityEngine;

public class GearedWheelMovement : MonoBehaviour
{
    public Transform sprite;
    
    [HideInInspector]
    public bool shouldMove = false;

    private void FixedUpdate()
    {
        if(Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f && shouldMove)
        {
            sprite.Rotate(Vector3.forward * PlayerMovement.instance.hMove * -1f);
        }
    }

}
