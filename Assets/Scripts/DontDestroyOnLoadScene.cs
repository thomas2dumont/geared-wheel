using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroyOnLoadScene : MonoBehaviour
{
    public GameObject[] objects;
    private void Awake()
    {
        foreach (var element in objects)
        {
            DontDestroyOnLoad(element);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name == "Level01")
            {
                foreach (var element in objects)
                {
                    Destroy(element);
                }
            }
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
