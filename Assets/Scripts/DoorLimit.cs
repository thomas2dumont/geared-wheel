using UnityEngine;

public class DoorLimit : MonoBehaviour
{
    public DoorMovement door;
    public bool stopingLimit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Door"))
        {
            door.stopMove = true;
            door.stopingLimit = stopingLimit;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Door"))
        {
            door.stopMove = false;
        }
    }
}
